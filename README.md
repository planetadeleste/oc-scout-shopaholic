# Laravel Scout for Shopaholic plugin

Shopaholic eCommerce extension: allows to search products, categories, tags, brands using laravel Scout.

## Description
> [Lovata.Shopaholic](https://octobercms.com/plugin/lovata-shopaholic) is required

After installing `PlanetaDelEste.ScoutShopaholic`, new FULLTEXT index will be added to `lovata_shopaholic_brands`, `lovata_shopaholic_categories`, `lovata_shopaholic_products` and `lovata_tagsshopaholic_tags` if `TagsShopaholic` is installed.

> **Note**: If you get DB error using FULLTEXT search, check db index to has same order and columns as `name, slug, external_id, code, preview_text, description, search_synonym, search_content` (last two are added only if `SearchShopaholic` is installed.

Plugin adds searchScout($sSearchString) method to  **[ProductCollection](https://github.com/lovata/oc-shopaholic-plugin/wiki/ProductCollection)**,  **[BrandCollection](https://github.com/lovata/oc-shopaholic-plugin/wiki/BrandCollection)**,  **[CategoryCollection](https://github.com/lovata/oc-shopaholic-plugin/wiki/CategoryCollection)**,  **[TagCollection](https://github.com/lovata/oc-shopaholic-plugin/wiki/TagCollection)**  classes.

```php
$obList =  ProductCollection::make()->search('test search');
```

## Installation

[Install](https://laravel.com/docs/5.8/scout#installation) Scout via the Composer package manager:

```php
composer require laravel/scout
```
After installing Scout, you should publish the Scout configuration using the  `vendor:publish`  Artisan command. This command will publish the  `scout.php`  configuration file to your  `config`  directory:

```php
php artisan vendor:publish --provider="Laravel\Scout\ScoutServiceProvider"
```


### Algolia Driver
If you want to use Algolia check [Laravel.Scout documentation](https://laravel.com/docs/5.8/scout#driver-prerequisites)

### MySQL Driver
Search Shopaholic Models using MySQL `FULLTEXT` Indexes
> **Note**: Any Models you plan to search using this driver must use a MySQL MyISAM or InnoDB table.

**Install with Composer**
```php
composer require yab/laravel-scout-mysql-driver
```

**Append the default configuration to  `config/scout.php`**

```php
 'mysql' => [
 'mode' => 'NATURAL_LANGUAGE',
 'model_directories' => [app_path()],
 'min_search_length' => 0,
 'min_fulltext_search_length' => 4,
 'min_fulltext_search_fallback' => 'LIKE',
 'query_expansion' => false
 ]
```

Set  `SCOUT_DRIVER=mysql`  in your  `.env`  file

Please note this Laravel Scout driver does not need to update any indexes when a Model is changed as this is handled natively by MySQL. Therefore you can safely disable queuing in  `config/scout.php`.

```php
 /*
 |--------------------------------------------------------------------------
 | Queue Data Syncing
 |--------------------------------------------------------------------------
 |
 | This option allows you to control if the operations that sync your data
 | with your search engines are queued. When this is set to "true" then
 | all automatic data syncing will get queued for better performance.
 |
 */
 'queue' => false,
```

In addition there is no need to use the  `php artisan scout:import`  command. However, if you plan to use this driver in either  `NATURAL_LANGUAGE`  or  `BOOLEAN`  mode you should first run the provided  [console command](https://github.com/yabhq/laravel-scout-mysql-driver#console-command)  to create the needed  `FULLTEXT`  indexes.

Full documentation of MySQL driver [here](https://github.com/yabhq/laravel-scout-mysql-driver)

