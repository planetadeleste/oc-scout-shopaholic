<?php return [
    'plugin' => [
        'name'        => 'Laravel Scout for Shopaholic',
        'description' => 'Scout search by products, categories, brands, tags',
    ],
    'tab'    => [
        'search_content'  => 'Search content',
        'search_settings' => 'Search',
    ],
    'field'  => [
        'search_synonym'       => 'Search synonyms',
        'search_content'       => 'Search content',
    ],
];