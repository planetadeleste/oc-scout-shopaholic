<?php

namespace PlanetaDelEste\ScoutShopaholic\Updates;

use DB;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateFulltextIndexes
 *
 * @package PlanetaDelEste\ScoutShopaholic\Updates
 */
class CreateFulltextIndexes extends Migration
{

    public function up()
    {
        if (Schema::hasTable('lovata_shopaholic_brands')) {
            $this->addIndexes('lovata_shopaholic_brands');
        }

        if (Schema::hasTable('lovata_shopaholic_categories')) {
            $this->addIndexes('lovata_shopaholic_categories');
        }

        if (Schema::hasTable('lovata_shopaholic_products')) {
            $this->addIndexes('lovata_shopaholic_products');
        }

        if (Schema::hasTable('lovata_tagsshopaholic_tags')) {
            $this->addIndexes('lovata_tagsshopaholic_tags');
        }

    }

    public function down()
    {

    }

    protected function addIndexes($tablename)
    {
        $index = str_replace('lovata_', 'fulltext_', $tablename).'_index';
        $columns = [
            'name',
            'slug',
            'external_id',
            'code',
            'preview_text',
            'description'
        ];
        if (Schema::hasColumns($tablename, ['search_synonym', 'search_content'])) {
            $columns[] = 'search_content';
            $columns[] = 'search_synonym';
        }

        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        $doctrineTable = $sm->listTableDetails($tablename);

        if (!$doctrineTable->hasIndex($index)) {
            DB::statement(sprintf("ALTER TABLE %s ADD FULLTEXT %s (%s)", $tablename, $index, join(',', $columns)));
        }
    }

}