<?php

namespace PlanetaDelEste\ScoutShopaholic\Classes\Helper;


use Laravel\Scout\Builder;
use Lovata\Toolbox\Traits\Helpers\TraitInitActiveLang;

class SearchHelper
{
    use TraitInitActiveLang;

    /** @var string */
    protected $sModel;

    /** @var string */
    protected $sSearch;

    /**
     * SearchHelper constructor.
     *
     * @param string $sModel
     */
    public function __construct($sModel)
    {
        $this->sModel = $sModel;
        $this->initActiveLang();
    }

    /**
     * @param $sSearch
     *
     * @return array|null
     */
    public function result($sSearch)
    {
        $sSearch = str_replace(' ', '+', $sSearch);
        $this->sSearch = trim($sSearch);

        if (!$this->validate()) {
            return null;
        }

        return $this->search()->get()->pluck('id')->all();
    }

    /**
     * Perform a search against the model's indexed data.
     *
     * @param  \Closure  $callback
     * @return \Laravel\Scout\Builder
     */
    protected function search($callback = null)
    {
        return app(
            Builder::class,
            [
                'model'      => new $this->sModel,
                'query'      => $this->sSearch,
                'callback'   => $callback,
                'softDelete' => config('scout.soft_delete', false)
            ]
        );
    }

    /**
     * Validate search model, search string, search settings
     * @return bool
     */
    protected function validate()
    {
        //Check model class
        if (empty($this->sModel) || !class_exists($this->sModel)) {
            return false;
        }

        //Check search string and search settings
        if (empty($this->sSearch)) {
            return false;
        }

        return true;
    }
}