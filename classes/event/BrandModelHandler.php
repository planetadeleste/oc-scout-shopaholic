<?php namespace PlanetaDelEste\ScoutShopaholic\Classes\Event;

use Lovata\Shopaholic\Classes\Collection\BrandCollection;
use Lovata\Shopaholic\Models\Brand;
use PlanetaDelEste\ScoutShopaholic\Classes\Helper\SearchHelper;

class BrandModelHandler
{
    /**
     * Add listeners
     */
    public function subscribe()
    {
        Brand::extend(
            function ($model) {
                /** @var Brand $model */
                $model->implement[] = 'PlanetaDelEste.ScoutShopaholic.Behaviors.SearchScoutModel';
            }
        );
        BrandCollection::extend(function ($obCollection) {
            /** @var BrandCollection $obCollection */
            $obCollection->addDynamicMethod('search', function ($sSearch) use ($obCollection) {

                /** @var SearchHelper $obSearchHelper */
                $obSearchHelper = app(SearchHelper::class, [Brand::class]);
                $arElementIDList = $obSearchHelper->result($sSearch);

                return $obCollection->intersect($arElementIDList);
            });
        });
    }
}