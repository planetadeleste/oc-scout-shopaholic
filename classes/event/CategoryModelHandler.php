<?php namespace PlanetaDelEste\ScoutShopaholic\Classes\Event;

use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\Shopaholic\Models\Category;
use PlanetaDelEste\ScoutShopaholic\Classes\Helper\SearchHelper;

class CategoryModelHandler
{
    /**
     * Add listeners
     */
    public function subscribe()
    {
        Category::extend(
            function ($model) {
                /** @var Category $model */
                $model->implement[] = 'PlanetaDelEste.ScoutShopaholic.Behaviors.SearchScoutModel';
            }
        );

        CategoryCollection::extend(function ($obCollection) {
            /** @var CategoryCollection $obCollection */
            $obCollection->addDynamicMethod('search', function ($sSearch) use ($obCollection) {


                /** @var SearchHelper $obSearchHelper */
                $obSearchHelper = app(SearchHelper::class, [Category::class]);
                $arElementIDList = $obSearchHelper->result($sSearch);

                return $obCollection->intersect($arElementIDList);
            });
        });
    }
}