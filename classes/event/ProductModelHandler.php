<?php namespace PlanetaDelEste\ScoutShopaholic\Classes\Event;

use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use PlanetaDelEste\ScoutShopaholic\Classes\Helper\SearchHelper;

/**
 * Class ProductModelHandler
 *
 * @package PlanetaDelEste\ScoutShopaholic\Classes\Event
 */
class ProductModelHandler
{
    /**
     * Add listeners
     */
    public function subscribe()
    {
        Product::extend(
            function ($model) {
                /** @var Product $model */
                $model->implement[] = 'PlanetaDelEste.ScoutShopaholic.Behaviors.SearchScoutModel';
            }
        );

        ProductCollection::extend(function ($obCollection) {
            /** @var ProductCollection $obCollection */
            $obCollection->addDynamicMethod('search', function ($sSearch) use ($obCollection) {
                /** @var SearchHelper $obSearchHelper */
                $obSearchHelper = app(SearchHelper::class, [Product::class]);
                $arElementIDList = $obSearchHelper->result($sSearch);

                return $obCollection->intersect($arElementIDList);
            });
        });
    }
}
