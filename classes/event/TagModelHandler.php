<?php namespace PlanetaDelEste\ScoutShopaholic\Classes\Event;

use PlanetaDelEste\ScoutShopaholic\Classes\Helper\SearchHelper;
use System\Classes\PluginManager;

/**
 * Class TagModelHandler
 * @package Lovata\SearchShopaholic\Classes\Event
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class TagModelHandler
{
    /**
     * Add listeners
     */
    public function subscribe()
    {
        if (!PluginManager::instance()->hasPlugin('Lovata.TagsShopaholic')) {
            return;
        }

        \Lovata\TagsShopaholic\Models\Tag::extend(
            function ($model) {
                $model->implement[] = 'PlanetaDelEste.ScoutShopaholic.Behaviors.SearchScoutModel';
            }
        );

        \Lovata\TagsShopaholic\Classes\Collection\TagCollection::extend(function ($obCollection) {
            /** @var \Lovata\TagsShopaholic\Classes\Collection\TagCollection $obCollection */
            $obCollection->addDynamicMethod('search', function ($sSearch) use ($obCollection) {

                /** @var SearchHelper $obSearchHelper */
                $obSearchHelper = app(SearchHelper::class, [\Lovata\TagsShopaholic\Models\Tag::class]);
                $arElementIDList = $obSearchHelper->result($sSearch);

                return $obCollection->intersect($arElementIDList);
            });
        });
    }
}
