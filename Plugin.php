<?php namespace PlanetaDelEste\ScoutShopaholic;

use Event;
use Laravel\Scout\ScoutServiceProvider;
use PlanetaDelEste\ScoutShopaholic\Classes\Event\BrandModelHandler;
use PlanetaDelEste\ScoutShopaholic\Classes\Event\CategoryModelHandler;
use PlanetaDelEste\ScoutShopaholic\Classes\Event\ProductModelHandler;
use PlanetaDelEste\ScoutShopaholic\Classes\Event\TagModelHandler;
use System\Classes\PluginBase;
use Yab\MySQLScout\Providers\MySQLScoutServiceProvider;

/**
 * ScoutShopaholic Plugin Information File
 */
class Plugin extends PluginBase
{
    /** @var array Plugin dependencies */
    public $require = ['Lovata.Shopaholic', 'Lovata.Toolbox'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => trans('planetadeleste.scoutshopaholic::lang.plugin.name'),
            'description' => trans('planetadeleste.scoutshopaholic::lang.plugin.description'),
            'author'      => 'PlanetaDelEste',
            'icon'        => 'oc-icon-search'
        ];
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot()
    {
        app()->register(ScoutServiceProvider::class);
        app()->register(MySQLScoutServiceProvider::class);

        Event::subscribe(BrandModelHandler::class);
        Event::subscribe(CategoryModelHandler::class);
        Event::subscribe(ProductModelHandler::class);
        Event::subscribe(TagModelHandler::class);
    }

}
